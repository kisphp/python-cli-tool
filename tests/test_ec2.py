import os
from datetime import datetime, timezone

from kpx.aws_ec2 import AwsResourceQuery


class Helper:
    def get_dir(self):
        return os.path.dirname(os.path.abspath(__file__))

    def create_files(self):
        self.create_config()
        self.create_credentials()

    def create_config(self):
        content = """[default]
region = us-east-1
output = json

[profile kp]
region = eu-central-1
output = json
"""
        with open(f'{self.get_dir()}/config', 'w') as obj:
            obj.write(content)

    def create_credentials(self):
        content = """[default]
aws_access_key_id = default-key
aws_secret_access_key = default-secret

[la]
aws_access_key_id = la-key
aws_secret_access_key = la-secret
"""
        with open(f'{self.get_dir()}/credentials', 'w') as obj:
            obj.write(content)

    def delete_files(self):
        os.remove(f'{self.get_dir()}/config')
        os.remove(f'{self.get_dir()}/credentials')


class MockClient():
    def describe_instances(self):
        return {
            'Reservations': [
                {
                    'Instances': [
                        {
                            'InstanceId': 'i-111',
                            'InstanceType': 't2-small',
                            'PrivateIpAddress': '10.10.0.210',
                            'LaunchTime': datetime.now(timezone.utc),
                            'State': {
                                'Name': 'running'
                            },
                            'Tags': [
                                {
                                    'Key': "Name",
                                    'Value': "demo-ec2"
                                }
                            ]
                        },
                        {
                            'InstanceId': 'i-222',
                            'InstanceType': 't2-small',
                            'PrivateIpAddress': '10.10.0.210',
                            'PublicIpAddress': '89.123.32.232',
                            'LaunchTime': datetime.now(timezone.utc),
                            'State': {
                                'Name': 'running'
                            },
                            'Tags': [
                                {
                                    'Key': "Description",
                                    'Value': "my description"
                                }
                            ]
                        },
                        {
                            'InstanceId': 'i-333',
                            'InstanceType': 't2-small',
                            'LaunchTime': datetime.now(timezone.utc),
                            'State': {
                                'Name': 'terminated'
                            },
                            'Tags': [
                                {
                                    'Key': "Description",
                                    'Value': "my description"
                                }
                            ]
                        },
                    ]
                }
            ]
        }


def test_list_ec2_instances(monkeypatch):
    def create_client():
        return MockClient()

    acm = AwsResourceQuery(create_client())

    header = acm.get_table_header()

    assert 'PrivateIP' in header

    out = acm.list_ec2_instances()

    assert len(out) == 3

    assert 'i-111' == list(out[0])[0]
    assert 'i-333' == list(out[2])[0]
    assert 'terminated' == list(out[2])[6]


def test_list_ec2(monkeypatch):
    def create_client():
        return MockClient()

    acm = AwsResourceQuery(create_client())

    header = acm.get_table_header()

    assert 'PrivateIP' in header

    acm.list_ec2('name=demo')
    # out = acm.list_ec2('name=demo')

    # assert len(out) == 1
    #
    # assert 'i-111' == list(out[0])[0]
    # assert 'i-333' == list(out[2])[0]
    # assert 'terminated' == list(out[2])[5]
