import json
import sys
from datetime import datetime, timezone

from io import StringIO
from kpx.k8s import list_nodes_info, SecretsQuery


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


class MockSession:
    def client(self, name):
        return MockClient()


class K8sMetadata():
    def __init__(self, name='obj1'):
        self.name = name
        self.labels = {
            'app.name': name,
            'node.kubernetes.io/instance-type': 'g6.2xlarge',
            'node.kubernetes.io/nodegroup': 'alfa',
        }
        self.creation_timestamp = datetime.now(timezone.utc)


class K8sMetadataSimple():
    def __init__(self):
        self.name = "obj1"
        self.labels = {
            'app.name': 'k8s.name',
        }
        self.creation_timestamp = datetime.now(timezone.utc)


class K8sStatus():
    def __init__(self):
        self.capacity = {
            'nvidia.com/gpu': 2,
            'cpu': 4,
            'memory': '16097728Ki',
            'ephemeral-storage': '20959212Ki',
        }


class K8sStatusSimple():
    def __init__(self):
        self.capacity = {
            'cpu': 4,
            'memory': '16097728Ki',
            'ephemeral-storage': '20959212Ki',
        }


class K8sTaint():
    def __init__(self, key, value, effect):
        self.key = key
        self.value = value
        self.effect = effect


class K8sSpec():
    def __init__(self):
        self.taints = [
            K8sTaint('group', 'gpu', 'NoSchedule')
        ]


class K8sSpecSimple():
    def __init__(self):
        self.taints = None


class K8sObject():
    def __init__(self, name):
        self.metadata = K8sMetadata(name)
        self.status = K8sStatus()
        self.spec = K8sSpec()


class K8sObjectEmpty():
    def __init__(self):
        self.metadata = K8sMetadataSimple()
        self.status = K8sStatusSimple()
        self.spec = K8sSpecSimple()


class MockClient:
    def __init__(self):
        self._items = [
            K8sObject('alfa'),
            K8sObject('beta-gpu'),
            K8sObject('gama'),
            K8sObject('delta'),
        ]

    @property
    def items(self):
        return self._items

    def list_node(self, *args, **kwargs):
        return self


class MockClientEmpty:
    def __init__(self):
        self._items = [
            K8sObjectEmpty(),
        ]

    @property
    def items(self):
        return self._items

    def list_node(self, *args, **kwargs):
        return self

class SubprocessMock():
    PIPE = None

    def Popen(self, *args, **kwargs):
        return self

    @property
    def stdout(self):
        return self

    def read(self):
        return self


class SubprocessMockStringData(SubprocessMock):
    def decode(self, *args, **kwargs):
        return json.dumps({
            "items":[
                {
                    "metadata":{
                        "name": "alfa-secret",
                        "namespace":"testing"
                    },
                    "type": "Opaque",
                    "stringData": {
                        "beta": "beta-pass"
                    }
                }
            ]
        })

class SubprocessMockData(SubprocessMock):
    def decode(self, *args, **kwargs):
        return json.dumps({
            "items":[
                {
                    "metadata":{
                        "name": "alfa-secret", "namespace":"testing"
                    },
                    "type": "Opaque",
                    "data": {
                        "alfa": "YmV0YQ=="
                    }
                }
            ]
        })

class SubprocessShowMockData(SubprocessMock):
    def decode(self, *args, **kwargs):
        return json.dumps({
            "metadata": {
                "name": "alfa-secret",
                "namespace": "testing"
            },
            "type": "Opaque",
            "data": {
                "alfa": "YmV0YQ=="
            },
            "stringData": {
                "beta": "beta-pass"
            }
        })

class SubprocessShowMockDataBrokenJson(SubprocessMock):
    def decode(self, *args, **kwargs):
        return 'no json'

def test_list_nodes(monkeypatch):
    def create_client():
        return MockClient()

    list_nodes_info(create_client(), 'name=gpu,group=alfa,type=g6,taints=gpu')


def test_aws_lb_empty(monkeypatch):
    def create_client_empty():
        return MockClientEmpty()

    list_nodes_info(create_client_empty(), '')

def test_list_secrets_string_data(monkeypatch):
    qs = SecretsQuery()

    def create_subprocess_mock():
        return SubprocessMockStringData()

    extra_options = ""

    qs.list_secrets(create_subprocess_mock(), extra_options)

def test_list_secrets_data(monkeypatch):
    qs = SecretsQuery()

    def create_subprocess_mock():
        return SubprocessMockData()

    extra_options = ""

    qs.list_secrets(create_subprocess_mock(), extra_options)


def test_show_secret(monkeypatch):
    qs = SecretsQuery()

    def create_subprocess_mock():
        return SubprocessShowMockData()

    extra_options = ""

    qs.show_secret(create_subprocess_mock(), 'alfa-secret', extra_options)


def test_show_secret_broken_json(monkeypatch):
    qs = SecretsQuery()

    def create_subprocess_mock():
        return SubprocessShowMockDataBrokenJson()

    extra_options = ""

    qs.show_secret(create_subprocess_mock(), 'alfa-secret', extra_options)
