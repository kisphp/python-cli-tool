import sys

from io import StringIO
from kpx.aws_ecr import ContainerRegistry
from contextlib import redirect_stdout


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


class MockClient():
    def describe_repositories(self):
        return {
            'repositories': [
                {
                    'repositoryName': 'alfa',
                    'repositoryUri': 'alfa/url'
                },
                {
                    'repositoryName': 'beta',
                    'repositoryUri': 'beta/url'
                },
            ]
        }

    def list_images(self, **kwargs):
        return {
            'imageIds': [
                {
                    'imageTag': '0.1.0',
                    'imageDigest': 'sha256:12312321'
                }
            ]
        }

    def describe_images(self, **kwargs):
        return {
            'imageDetails': [
                {
                    'imageSizeInBytes': 1024*1024*1024+500*1024
                }
            ]
        }


def test_repos_list(monkeypatch):
    def create_client():
        return MockClient()

    ecr = ContainerRegistry(create_client())

    f = StringIO()
    with redirect_stdout(f):
        ecr.get_repositories()

    output = f.getvalue()
    assert 'alfa' in output


def test_image_tags_list(monkeypatch):
    def create_client():
        return MockClient()

    account_id = 11111
    ecr = ContainerRegistry(create_client())

    f = StringIO()
    with redirect_stdout(f):
        ecr.get_images('alfa', account_id)

    output = f.getvalue()
    assert '0.1.0' in output
