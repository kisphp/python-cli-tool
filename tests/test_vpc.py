import sys

from io import StringIO
from kpx.aws_vpc import VpcManager
from contextlib import redirect_stdout


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


class MockSession:
    def client(self, name):
        return MockClient()


class MockClient:
    def describe_subnets(self, *args, **kwargs):
        return {
            'Subnets': [
                {
                    'SubnetId': 'subnet-987',
                    'CidrBlock': "10.10.0.0/18",
                    'AvailableIpAddressCount': 2048,
                    'AvailabilityZone': 'us-east-1a',
                }
            ]
        }

    def describe_vpcs(self):
        return {
            'Vpcs': [
                {
                    'VpcId': 'vpc-123123123132',
                    'CidrBlock': '10.10.0.0/16',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': ['Demo']
                        }
                    ]
                }
            ]
        }


def test_vpc_list(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    ecr = VpcManager(create_session())

    f = StringIO()
    with redirect_stdout(f):
        ecr.get_vpc_list(create_client())

    output = f.getvalue()
    assert 'vpc-123123' in output


def test_vpc_subnets(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    ecr = VpcManager(create_session())

    f = StringIO()
    with redirect_stdout(f):
        ecr.get_vpc_subnets('vpc-123')

    output = f.getvalue()
    assert 'subnet-987' in output

# def test_vpc():
#     vpc()
#
# def test_subnets():
#     vpc('vpc-123')
