import sys

from io import StringIO
from kpx.aws_lb import AwsLb


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


class MockSession:
    def client(self, name):
        return MockClient()


class MockClientEmpty:
    def list_resource_record_sets(self):
        return {
            'LoadBalancers': []
        }


class MockClient:
    @staticmethod
    def describe_load_balancers(PageSize=10):
        return {
            'LoadBalancers': [
                {
                    'LoadBalancerName': 'aws-lb-1',
                    'VpcId': 'aws-vpc-1',
                    'Type': 'network',
                    'DNSName': 'test',
                    'State': {
                        'Code': 'active'
                    }
                },
            ]
        }


def test_aws_lb(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    awslb = AwsLb(create_client())
    awslb.get_load_balancers()

# def test_route53_zoneless(monkeypatch):
#     def create_session():
#         return MockSession()
#
#     def create_client():
#         return MockClient()
#
#     Route53.list_r53(create_client())
#
# def test_route53_zoneless_empty(monkeypatch):
#     def create_session():
#         return MockSession()
#
#     def create_client():
#         return MockClientEmpty()
#
#     Route53.list_r53(create_client())
