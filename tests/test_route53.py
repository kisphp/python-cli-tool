import sys

from io import StringIO
from kpx.aws_route53 import Route53


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio  # free up some memory
        sys.stdout = self._stdout


class MockSession:
    def client(self, name):
        return MockClient()


class MockClientEmpty:
    def list_resource_record_sets(self):
        return {
            'HostedZones': []
        }


class MockClient:
    def list_resource_record_sets(self, *args, **kwargs):
        return {
            'ResourceRecordSets': [
                {
                    'Id': 'r1-r53',
                    'Name': 'r1',
                    'Type': 'test',
                    'AliasTarget': {
                        'DNSName': 'r1.dns-name'
                    },
                    'ResourceRecords': [
                        {
                            'Value': 'A',
                        },
                        {
                            'Value': 'B',
                        },
                    ]
                },
                {
                    'Id': 'r2-r53',
                    'Name': 'r2',
                    'Type': 'test',
                },
            ]
        }

    def list_hosted_zones(self, *args, **kwargs):
        return {
            'HostedZones': [{
                'Id': '/hostedzone/alfa',
                'Name': 'alfa',
                'ResourceRecordSetCount': 10
            }]
        }

class ZoneIdEmpty():
    def list_hosted_zones(self, *args, **kwargs):
        return {
            'HostedZones': []
        }

def test_route53(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    Route53.list_r53(create_client(), 'asd')

def test_route53_filter(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    Route53.list_r53(create_client(), 'asd', 'r2')


def test_route53_zoneless(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClient()

    Route53.list_r53(create_client())


def test_route53_zoneless_empty(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return MockClientEmpty()

    Route53.list_r53(create_client())

def test_route53_zoneid_empty(monkeypatch):
    def create_session():
        return MockSession()

    def create_client():
        return ZoneIdEmpty()

    Route53.list_r53(create_client())
