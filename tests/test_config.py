import os

from kpx.main import AwsConfigManager, cfg, cred


class Helper:
    @staticmethod
    def get_dir():
        return os.path.dirname(os.path.abspath(__file__))

    def create_files(self):
        self.create_config()
        self.create_credentials()

    def create_config(self):
        content = """[default]
region = us-east-1
output = json

[profile kp]
region = eu-central-1
output = json
"""
        with open(f'{self.get_dir()}/config', 'w') as obj:
            obj.write(content)

    def create_credentials(self):
        content = """[default]
aws_access_key_id = default-key
aws_secret_access_key = default-secret

[la]
aws_access_key_id = la-key
aws_secret_access_key = la-secret
"""
        with open(f'{self.get_dir()}/credentials', 'w') as obj:
            obj.write(content)

    def delete_files(self):
        os.remove(f'{self.get_dir()}/config')
        os.remove(f'{self.get_dir()}/credentials')


def test_get_config():
    h = Helper()
    h.create_files()

    acm = AwsConfigManager(
        f'{h.get_dir()}/credentials',
        f'{h.get_dir()}/config'
    )

    assert 'tests/config' in acm.file_config
    assert 'tests/credentials' in acm.file_credentials

    obj = acm.get_config('default')

    assert obj['region'] == 'us-east-1'
    assert obj['output'] == 'json'

    h.delete_files()


def test_create_config():
    h = Helper()
    h.create_files()

    acm = AwsConfigManager(
        f'{h.get_dir()}/credentials',
        f'{h.get_dir()}/config'
    )

    obj = acm.get_config('demo')
    assert obj == {}

    acm.update_config('demo', 'demo-region', 'demo-output')
    acm.write_config_file()

    obj = acm.get_config('demo')

    assert obj['region'] == 'demo-region'
    assert obj['output'] == 'demo-output'

    h.delete_files()


def test_get_credentials():
    h = Helper()
    h.create_files()

    acm = AwsConfigManager(
        f'{h.get_dir()}/credentials',
        f'{h.get_dir()}/config'
    )

    assert 'tests/config' in acm.file_config
    assert 'tests/credentials' in acm.file_credentials

    obj = acm.get_credentials('default')

    assert obj['aws_access_key_id'] == 'default-key'
    assert obj['aws_secret_access_key'] == 'default-secret'

    h.delete_files()


def test_create_credentials():
    h = Helper()
    h.create_files()

    acm = AwsConfigManager(
        f'{h.get_dir()}/credentials',
        f'{h.get_dir()}/config'
    )

    assert 'tests/config' in acm.file_config
    assert 'tests/credentials' in acm.file_credentials

    obj = acm.get_credentials('demo')
    assert obj == {}

    acm.update_credentials('demo', 'demo-user', 'demo-pass')
    acm.write_credentials_file()

    obj = acm.get_credentials('demo')

    assert obj['aws_access_key_id'] == 'demo-user'
    assert obj['aws_secret_access_key'] == 'demo-pass'

    h.delete_files()


def test_cfg():
    cfg('profile_name', 'region', 'output')


def test_cred():
    cred('profile', 'key', 'secret')
