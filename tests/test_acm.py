import os
from datetime import datetime, timezone
from kpx.aws_acm import CertificateManager


class Helper:
    def get_dir(self):
        return os.path.dirname(os.path.abspath(__file__))

    def create_files(self):
        self.create_config()
        self.create_credentials()

    def create_config(self):
        content = """[default]
region = us-east-1
output = json

[profile kp]
region = eu-central-1
output = json
"""
        with open(f'{self.get_dir()}/config', 'w') as obj:
            obj.write(content)

    def create_credentials(self):
        content = """[default]
aws_access_key_id = default-key
aws_secret_access_key = default-secret

[la]
aws_access_key_id = la-key
aws_secret_access_key = la-secret
"""
        with open(f'{self.get_dir()}/credentials', 'w') as obj:
            obj.write(content)

    def delete_files(self):
        os.remove(f'{self.get_dir()}/config')
        os.remove(f'{self.get_dir()}/credentials')


def date_convert(timestamp_str):
    timestamp = float(timestamp_str)  # Convert string to float
    return datetime.fromtimestamp(timestamp, tz=timezone.utc)

class MockClient():
    def list_certificates(self):
        return {
            "CertificateSummaryList": [
                {
                    "CertificateArn": "arn:aws:acm:us-east-1:12345EXAMPLE:certificate/faca5ab5-df18-440c-839e-784d47c0d475",
                    "DomainName": "12345EXAMPLE.example.com",
                    "SubjectAlternativeNameSummaries": [
                        "12345EXAMPLE.example.com",
                        "www.12345EXAMPLE.example.com"
                    ],
                    "HasAdditionalSubjectAlternativeNames": False,
                    "Status": "ISSUED",
                    "Type": "AMAZON_ISSUED",
                    "KeyAlgorithm": "RSA-2048",
                    "KeyUsages": [
                        "DIGITAL_SIGNATURE",
                        "KEY_ENCIPHERMENT"
                    ],
                    "ExtendedKeyUsages": [
                        "TLS_WEB_SERVER_AUTHENTICATION",
                        "TLS_WEB_CLIENT_AUTHENTICATION"
                    ],
                    "InUse": False,
                    "RenewalEligibility": "INELIGIBLE",
                    "NotBefore": date_convert(1736640000.0),
                    "NotAfter": date_convert(1770767999.0),
                    "CreatedAt": date_convert(1736675836.128),
                    "IssuedAt": date_convert(1736675882.658)
                },
                {
                    "CertificateArn": "arn:aws:acm:us-east-1:12345EXAMPLE:certificate/5aa2fe0f-d209-4006-9b62-578c9540e10f",
                    "DomainName": "beta.12345EXAMPLE.example.com",
                    "SubjectAlternativeNameSummaries": [
                        "beta.12345EXAMPLE.example.com"
                    ],
                    "HasAdditionalSubjectAlternativeNames": False,
                    "Status": "ISSUED",
                    "Type": "AMAZON_ISSUED",
                    "KeyAlgorithm": "RSA-2048",
                    "KeyUsages": [
                        "DIGITAL_SIGNATURE",
                        "KEY_ENCIPHERMENT"
                    ],
                    "ExtendedKeyUsages": [
                        "TLS_WEB_SERVER_AUTHENTICATION",
                        "TLS_WEB_CLIENT_AUTHENTICATION"
                    ],
                    "InUse": False,
                    "RenewalEligibility": "INELIGIBLE",
                    "NotBefore": date_convert(1736640000.0),
                    "NotAfter": date_convert(1770767999.0),
                    "CreatedAt": date_convert(1736675836.133),
                    "IssuedAt": date_convert(1736675880.865)
                },
                {
                    "CertificateArn": "arn:aws:acm:us-east-1:12345EXAMPLE:certificate/3c83fc78-76cd-4da8-8080-a6e5bf9de1b7",
                    "DomainName": "alfa.12345EXAMPLE.example.com",
                    "SubjectAlternativeNameSummaries": [
                        "alfa.12345EXAMPLE.example.com"
                    ],
                    "HasAdditionalSubjectAlternativeNames": False,
                    "Status": "ISSUED",
                    "Type": "AMAZON_ISSUED",
                    "KeyAlgorithm": "RSA-2048",
                    "KeyUsages": [
                        "DIGITAL_SIGNATURE",
                        "KEY_ENCIPHERMENT"
                    ],
                    "ExtendedKeyUsages": [
                        "TLS_WEB_SERVER_AUTHENTICATION",
                        "TLS_WEB_CLIENT_AUTHENTICATION"
                    ],
                    "InUse": False,
                    "RenewalEligibility": "INELIGIBLE",
                    "NotBefore": date_convert(1736640000.0),
                    "NotAfter": date_convert(1770767999.0),
                    "CreatedAt": date_convert(1736675836.167),
                    "IssuedAt": date_convert(1736675880.761)
                }
            ],
            "ResponseMetadata": {
                "RequestId": "c4222db5-2260-468e-bcde-d6f3256623ac",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "x-amzn-requestid": "c4222db5-2260-468e-bcde-d6f3256623ac",
                    "content-type": "application/x-amz-json-1.1",
                    "content-length": "1968",
                    "date": "Sun, 12 Jan 2025 10:03:12 GMT"
                },
                "RetryAttempts": 0
            }
        }


def test_list_certificates(monkeypatch):
    def create_client():
        return MockClient()

    acm = CertificateManager(create_client())

    table = acm.get_certificates()

    assert len(table.rows) == 3

    assert 'beta.12345EXAMPLE.example.com' == table.rows[1][0]
