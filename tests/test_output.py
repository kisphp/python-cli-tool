import sys

from io import StringIO
from kpx.utils import Output
from contextlib import redirect_stdout


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


def test_output():
    f = StringIO()
    with redirect_stdout(f):
        Output.header('this is my header message')

    output = f.getvalue()
    assert 'my header' in output


def test_success():
    f = StringIO()
    with redirect_stdout(f):
        Output.success('this is my success message')

    output = f.getvalue()
    assert 'my success' in output


def test_error():
    f = StringIO()
    with redirect_stdout(f):
        Output.error('this is my error message')

    output = f.getvalue()
    assert 'my error' in output
