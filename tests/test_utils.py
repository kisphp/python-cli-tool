import sys

from datetime import datetime, timedelta, timezone
from kpx.utils import convert_seconds_to_age

def test_one_hour(monkeypatch):
    delta = convert_seconds_to_age(3600)
    assert delta == '1h'

def test_one_hour_and_25_minutes(monkeypatch):
    delta = convert_seconds_to_age(5100)
    assert delta == '1h25m'

def test_one_hour_and_25_minutes_30_seconds(monkeypatch):
    delta = convert_seconds_to_age(5130)
    assert delta == '1h25m'

def test_one_4_days(monkeypatch):
    delta = convert_seconds_to_age(3600 * 24 * 4)
    assert delta == '4d'

def test_one_4_days_and_7_hours(monkeypatch):
    delta = convert_seconds_to_age(3600 * 24 * 4 + 3600 * 7)
    assert delta == '4d7h'

def test_one_2_days_8_hours_11_minutes(monkeypatch):
    delta = convert_seconds_to_age(3600 * 24 * 2 + 3600 * 8 + 11 * 60)
    assert delta == '2d8h'
