from kpx.utils import sizeof_fmt


def test_bytes():
    assert '100.0B' == sizeof_fmt(100)


def test_Kbytes():
    assert '1.1KiB' == sizeof_fmt(1100)


def test_Mbytes():
    assert '1.0MiB' == sizeof_fmt(1024*1024)


def test_Gbytes():
    assert '1.0GiB' == sizeof_fmt(1024*1024*1024)


def test_over_value():
    assert '1024.0YiB' == sizeof_fmt(1024*1024*1024*1024*1024*1024*1024*1024*1024)
