.PHONY: help install save docs test build

.DEFAULT_GOAL := help

help: ## Display this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "[36m%-30s[0m %s\n", $$1, $$2}'

install: ## Install packages
	pip install -r requirements.txt

save: ## Export packages list
	pip freeze | sort > requirements.txt

docs: ## Generate USAGE.md file
	cat README-TPL.md > README.md
	typer kpx.main utils docs --name kpx >> README.md

test: ## Run pytest and generate html coverage report
	poetry run -- coverage run --source=kpx -m pytest -v
	poetry run -- coverage html
	poetry run -- coverage report

build:
	poetry install
	poetry export --without-hashes -o requirements.txt
	poetry build
