[![pipeline status](https://gitlab.com/kisphp/python-cli-tool/badges/main/pipeline.svg)](https://gitlab.com/kisphp/python-cli-tool/-/commits/main)
[![coverage report](https://gitlab.com/kisphp/python-cli-tool/badges/main/coverage.svg)](https://gitlab.com/kisphp/python-cli-tool/-/commits/main)
[![Latest Release](https://gitlab.com/kisphp/python-cli-tool/-/badges/release.svg)](https://gitlab.com/kisphp/python-cli-tool/-/releases)

## Install

```bash
# Install or update it from pip
pipx install -U kpx

# Install or update it from gitlab
pipx install -U kpx --index-url https://gitlab.com/api/v4/projects/24038501/packages/pypi/simple
```

## Contribute

[Install poetry globally](https://python-poetry.org/docs/#installing-with-the-official-installer)

```shell
curl -sSL https://install.python-poetry.org | python3 -
```

Clone the project

```shell
git clone https://gitlab.com/kisphp/kpx.git 
```

Create virtual env inside the project directory

```shell
cd kpx

# create venv
python3 -m venv venv # first venv is the python module called venv, second one is the name of our local virtual environment
```

Install local dependencies

```shell
poetry install
```

Run application from poetry 

```shell
poetry run -- kpx # this will show the full list of available commands
poetry run -- kpx ec2 # will list all ec2 instances in your aws account
poetry run -- kpx ec2 -f name=gpu,state=running # list ec2 instances with filtered results
```

Exit from virtual environment

```shell
deactivate # type this in the terminal window and click Enter key
```
